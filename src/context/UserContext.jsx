import { createContext, useContext, useState } from "react";
import { storageRead } from "../storageUtil/storage";

//exposes the user state
const UserContext = createContext()
export const useUser = () => {
    return useContext(UserContext)
}
//defining the state of the user
const UserProvider = ({ children }) => {

    const [user, setUser] = useState(storageRead('user'))

    const state = {
        user,
        setUser
    }

    return (
        <UserContext.Provider value={state}>
            {children}
        </UserContext.Provider>
    )
}
export default UserProvider