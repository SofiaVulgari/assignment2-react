import UserProvider from "./UserContext"

//What is rendered here is displayed in the browser 
const AppContext = ({ children }) => {

    return (
        //Uses the provider from UserContext.jsx
        <UserProvider>
            {children}
        </UserProvider>
    )
}
export default AppContext