import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import StartUp from './pages/StartUp';
import Translation from "./pages/Translation";
import Profile from "./pages/Profile";
import Navbar from './components/Navbar/Navbar';

/**
 * Main app, also handles the routing
 */
function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={<StartUp />} />
          <Route path="/Translation" element={<Translation />} />
          <Route path="/Profile" element={<Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
