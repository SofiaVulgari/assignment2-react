import { createHeaders } from './index';

const apiUrl = process.env.REACT_APP_API_URL

/**
 * Function that updates with a new translation to a specific user
 */
export const addTranslation = async (user, myWords) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, user.translations.push(myWords)]
            })
        })

        if (!response.ok) {
            throw new Error('Could not update new translation')
        }

        const result = await response.json()
        return [null, result]

    }
    catch (error) {
        return [error.message, null]
    }

}

/**
 * Function that deletes all translations for a specific user
 */
export const translationClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })

        if (!response.ok) {
            throw new Error('Could not delete translation history')
        }

        const result = await response.json()
        return [null, result]
    } catch (error) {
        return [error.message, null]
    }
}
